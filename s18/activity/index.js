// Sum
	
	function addNum(x , y) {
		let sum =  x + y;
		console.log("Displayed sum of "+ x + " and " + y )
		console.log(sum);
	};

	addNum(5,15);

//Invoke and pass

	function invokeaddNum(addNum){
		addNum();
	}

// Difference

	function subNum(x , y) {
		let difference =  x - y;
		console.log("Displayed difference of "+ x + " and " + y )
		console.log(difference);
	};

	subNum(20,5);

	//Invoke and pass

		function invoke_subNum(subNum){
			subNum();
		}

// Product
	function multiplyNum( factor1 , factor2) {
		let multiplyResult = factor1 * factor2;
		console.log("The product of " + factor1 + " and " + factor2 + ":");
		console.log(multiplyResult);
		return "The product of" + factor1 + " and " +  factor2 + "\n" + multiplyResult
	}

/*	multiplyNum(50, 10);*/

// Storing the multiplyNUm

	let product = multiplyNum(50, 10)

// Quotient
	function divideNum( dividend , divisor) {
		let divideResult = dividend / divisor;
		console.log("The quotient of " + dividend + " and " + divisor + ":");
		console.log(divideResult);
		return "The quotient of" + dividend + " and " +  divisor + "\n" + divideResult
	}

	// divideNum(50, 10);

//Storing the divideNum
	let quotient = divideNum(50, 10)

// Area
	function getCircleArea(r) {
		let area = 3.1416*(r**2);
		console.log("The result of getting the area of a circle with " + r + " radius:")
		console.log(area)
		return "The result of getting the area of a circle with " + r + " radius:" + "\n" + area
	}

//Storing getCircleArea

	let circleArea = getCircleArea(15)

// Average
	function getAverage(num1, num2, num3, num4) {
		let average = (num1 + num2 + num3 + num4)/4
		console.log("The average of " + num1 +"," + num2 +"," + num3 +" and " + num4 + ":")
		console.log(average)
		return "The average of " + num1 +"," + num2 +"," + num3 +" and " + num4 + ":" + "\n" + average
	}

//Storing getAverage

	let averageVar=getAverage(20, 40, 60, 80)


// Pass/Fail
	function checkIfPassed(yourScore, totalScore) {
		percentage = (yourScore/totalScore)*100
		isPassed = percentage >= 75;
		console.log("Is "+ yourScore + "/" +totalScore + " a passing score?");
		console.log(isPassed);
		return "Is "+ yourScore + "/" +totalScore + " a passing score?" + "\n" + isPassed
	}

//Storing checkIfPassed
	let isPassingScore = checkIfPassed(38, 50)

//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}