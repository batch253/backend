// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");
// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


// Database connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin123@batch253-esteves.s0qft0l.mongodb.net/s36?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection; 
// If a connection error occurred, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

app.use("/tasks", taskRoute);

if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;