const express = require("express");
const router = express.Router();


const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});

router.get("/details", (req, res) => {
	// userController.getProfile(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))

});

//Route to enroll user to a course
// router.post("/enroll", (req, res) => {
// 	let data = {
// 		userId: req.body.userId,
// 		courseId: req.body.courseId
// 	}


// 	userController.enroll(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
// });
//===========ACTIVITY======================

router.post("/enroll", auth.verify,  (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: userData.id,
		isAdmin : userData.isAdmin,
		courseId: req.body.courseId
	};

	if(userData.isAdmin === false){
		userController.enroll(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	}else {
		res.send(false);
	}
});

module.exports = router;