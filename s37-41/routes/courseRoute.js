const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	// const data = {
	// 	course: req.body,
	// 	isAdmin: auth.decode(req.headers.authorization).isAdmin
	// }

	// courseController.addCourse(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}

});

//Routes for retrieving all the courses

router.get("/all", auth.verify, (req, res) => { //need middleware auth

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.getAllCourses().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});


//Route for retrievong all the Actvie courses
//Middleware for verifying JWT is not required because users who arent't logg

router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

router.get("/:courseId/archive", (req, res) => {

	console.log(req.params);
	// Route for retrieving a specific course
	// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
	// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e		// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

router.put("/:courseId/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}



});

//============ACTIVITY================
router.patch("/:courseId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}

});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;