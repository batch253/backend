const express = require("express");
const mongoose = require("mongoose")
const cors = require("cors")
const userRoute = require("./routes/userRoute")
const app = express();
const port = process.env.PORT || 4000;
const db = mongoose.connection;
const courseRoute = require("./routes/courseRoute")

mongoose.connect("mongodb+srv://admin:admin123@batch253-esteves.s0qft0l.mongodb.net/course-bookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology: true
});

db.once("open", () => console.log("Now connected to MongoDB Atlas."));

app.use(express.json());
app.use(express.urlencoded({extended : true}));

app.use("/users", userRoute);
app.use("/courses", courseRoute);



if(require.main === module){
	app.listen(port, () => {
		console.log(`API is now online on port ${port}`)
	});
}

module.exports = app;