const Course = require("../models/Course");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/
module.exports.addCourse = (reqBody) => {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});

		return newCourse.save().then(course => true)
			.catch(err => false);
};
// Retrieve all courses
		/*
			Steps:
			1. Retrieve all the courses from the database
		*/
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => result)
		.catch(err => err);
};

module.exports.getAllActive = () => {
	return Course.find({IsActive: true}).then(result => result)
		.catch(err => err);
};

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => result)
		.catch(err => err);
};

// Update a course
/*
Steps:
1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body

module.exports.updateCourse = (reqParams, reqBody) => {
	// Specify the fields/properties of the document to be updated

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/*
		Syntax: 
			Modelname.findByIdAndUpdate(documentId, updatesToBeApplied).then(statement)
	*/

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(course => true).catch(err =>err);
};


//===================ACTIVITY==================
module.exports.archiveCourse = (reqParams, reqBody) => {
	// Specify the fields/properties of the document to be updated

	let updatedCourse = {
		IsActive: reqBody.IsActive
	};

	/*
		Syntax: 
			Modelname.findByIdAndUpdate(documentId, updatesToBeApplied).then(statement)
	*/

	return Course.findByIdAndUpdate(reqParams.courseId, true).then(course => true).catch(err =>err);
};