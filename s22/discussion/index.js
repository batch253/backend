console.log("Guten Morgen");

//Array Methods

/*
     1. Mutator Methods
     	- seeks to modify the contents of an array.
     	 - mutator methods are functions that mutate or change an array after they are created. These methods manipulate the original array by perfoming various tasks such as adding or removing elements.
 */

let fruits = ["Apple", "Banana", "Orange", "Mango"];

/*
	push()
		- adds an element at the end of an array and it returns array's length

*/

console.log("Current Fruits Array");
console.log(fruits);

//Adding element/s

let fruitLength = fruits.push("Pineapple");
console.log(fruitLength);
console.log("Mutated Array using push() method");
console.log(fruits);


fruits.push("Avocado", "Jackfruit", "Guava", "Pomelo");
console.log(fruits);

/*
	pop()
	-removes the last element in our array and returns the removed element (when valye is passed in another variable).
*/

let removeFruit = fruits.pop();
console.log(removeFruit)
console.log("Mutated Array uisng pop() method")
console.log(fruits)
fruits.pop();
console.log(fruits);


/* 
    unshift()
        - adds one or more elements AT THE BEGINNING of an array and returns the length of the array(when value is passed in another variable).

        Syntax:
            arrayName.unshift(element);
            arrayName.unshift(elementA, elementB);
*/


let unshiftLength = fruits.unshift("Lemon");
console.log(unshiftLength);
console.log("Mutated Array using unshift() method");
console.log(fruits);

fruits.unshift("Kiwi", "Strawberry");
console.log(fruits);

/* 
    shift()
        - removes an element AT THE BEGINNING of our array and returns the removed elementwhen stored in avariable.
*/

let shiftFruit = fruits.shift();
console.log(shiftFruit);
console.log("Muated Array using shift() method");
console.log(fruits);

/*
	splice()
		- allows to simultaneously remove elements from a specified index number and adds an element

		Syntax: 
			- arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)

*/


let spliceFruit = fruits.splice(1,2, "Cherry", "Dragon Fruit");
console.log(spliceFruit);
console.log("Mutated Array using splice() method");
console.log(fruits);


//using splice() without adding elements
fruits.splice(5, 3);
console.log(fruits);
 

/*
	sort()
		-rearranges the array elements in alphanumeric order

		Syntax:
			arrayName.sort();
*/


fruits.sort();
console.log("Mutated Array using sort() methpd");
console.log(fruits);

let mixedArr = [50, 10, 1, 8, "Emvir", "Adrian", undefined, [], "Marvin", "Joseph"];


console.log(mixedArr.sort());

/*
	reverse()
		- reverses the order  of the element in an array

		Syntax:
		 arrayName.reverse();
*/

fruits.reverse();
console.log("Mutated Array using reverse() method");
console.log(fruits)

//Shorthand for descending order
//console.log(fruits.sort().reverse());


// function registerFruit (fruitName) {
//  		let doesFruitExist = fruits.includes(fruitName);

//  		if(doesFruitExist) {
//  			alert(fruitName + " is already on our inventory")
//  		} else {
//  			fruits.push(fruitName)
//  			alert("Fruit is now listed in our inventory")
//  		}
//  	}
//  registerFruit("Banana")

/*
	2. Non-Mutator Methods
		- these are methods/functions that do not modify or change an array they are created. These methods also do not manipulate the original arrat but still performs various tasks such as returning element from an array.
*/

let countries = ["US" ,"PH", "CAN", "SG", "TH", "PH", "FR", "DK"];
console.log(countries);

/*
	indexOf()
	 - returns the index number of the FIRST MATCHING element found in an array. If no macth was found, the result will be -1. The search process will be done from our first element proceeding to the last element.

	 Syntax: 
	 	arrayName:indexOf(serchValue);
	 	arrayName.indexof(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf("PH");
console.log(firstIndex);

firstIndex = countries.indexOf("PH",4);
console.log(firstIndex);

firstIndex = countries.indexOf("PH",7);
console.log(firstIndex);

firstIndex = countries.indexOf("DE");
console.log(firstIndex);

firstIndex = countries.indexOf("DE", -1);
console.log(firstIndex);

/*
	lastIndexOf()
		- returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first element.

			Syntax :
				arrayName.lastIndexOf(searchValue);
				arrayName.lastIndexOf(searchValue, startingFromIndex)
*/

let lastIndex =countries.lastIndexOf("PH");
console.log("Resu;t of lastIndexOf(): " + lastIndex) ;


lastIndex =countries.lastIndexOf("PH", 4);
console.log("Resu;t of lastIndexOf(): " + lastIndex) ;

/* 
    slice()
        - portions/slices elements from our array and return a new array.

        Syntax:
            arrayName.slice(startingIndex);
            arrayName.slice(startingIndex, endingIndex);
*/

console.log(countries);

// slicing from specified index to last element
let sliceArrayA = countries.slice(2);
console.log("Result of slice() method: ")
console.log(sliceArrayA)
console.log(countries);

// slicing from specified index to another specified index
let sliceArrayB = countries.slice(0,4);
console.log("Result of slice() method: ")
console.log(sliceArrayB)

// slicing fstarting from the last index
let sliceArrayC = countries.slice(-3);
console.log("Result of slice() method: ")
console.log(sliceArrayC)

/*
	3. Iteration Methods
			- are loops designed to perform repetitive tasks on arrays. This is useful for manipulating array data resulting in complex tasks.
			- normally work with a function is supplied as an argument.
			- aims to evaluate each element in an array.
*/

/*
	forEach()
		- similar for loop that iterates on each array element

			Syntax: 
				arrayName.forEach(function(individualElements) { statement/business logic })

*/

/*
	map()
		- Iterates on each element AND returns new array with different values depending on the result of the function's operation
		- This is useful for performing tasks where mutating/changing the elements are required
		- Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation

		Syntax:
		 	let/const resultArray = arrayName.map(function(individualELement){
		 		return statement;
		 	})
*/

let numbers = [1 , 2 , 3 , 4 , 5];

let numberMap = numbers.map(function(number){
	return number * number;
});


console.log("Result of map() method: ");
console.log(numbers);
console.log(numberMap);

/*
	every()
		- checks if all elements in an array met the given condition. returns a "true" value if all elements meet the condition and "false" if otherwise.

		Syntax:
			let/const resultArray = arrayName.every(function(individualelement){
				return expression/condition
			});

*/

let allValid = numbers.every(function(number){
	return(number > 6);
});

console.log("Result of every () method:")
console.log(allValid)

/*
	
	some()
		- checks if at least one element in the array meet the given condition. Returns a "true" value is at least one element meets the given condition and false if otherwise.

	Syntax:
		let/const resultArray = arrayName.some(function(individualelement){
			return expression/condition
		});


*/

let someValid = numbers.some(function(number){

})
/*
	
	some()
		- checks if at least one element in the array meet the given condition. Returns a "true" value is at least one element meets the given condition and false if otherwise.

	Syntax:
		let/const resultArray = arrayName.some(function(individualelement){
			return expression/condition
		});


*/


/*
	filter()
		- returns a new array that contains elements which meet the given condiiton. Returns an empty array if no elements were found that satisfy the given condition.

	Syntax:
		let/const resultArray = arrayName.filter(function(individualelement){
			return expression/condition
		});
*/


/*

	includes()
		- checks if the argument passed can be found in the array.
		- methods can be "chained" using them one after another. The result of the first methid is being used on the second method until all the "chained" methods have been resolved.

*/

/*
reduce()
 Evaluates elements from left to right and returns/reduces the array into a single value

        - The "accumulator" parameter in the function stores the result for every iteration of the loop

- The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
        - How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elements
            4. The loop repeats step 1-3 until all elements have been work
            - Syntax
                let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
                    return expression/operation
                })
*/

let iteration = 0;
let reducedArray = numbers.reduce(function(acc, curr){
	console.warn(`Current iteration: ${++iteration}`);
	console.log(`accumulator: ${acc}`);
	console.log(`currentValue: ${curr}`)

	// operation to return/reduc the array into a single value
	return acc + curr
});

console.log("Result of reduce() method: " + reducedArray)

let list = ["Hello", "Again", "batch 253"];

let reducedJoin =  list.reduce(function(acc,curr){
	return `${acc} ${curr}`
})
console.log(reducedJoin)