// Use the "require" directive to load the express module/package
		// A "module" is a software component or part of a program that contains one or more routines
		// This is used to get the contents of the express package to be used by our application
		// It also allows us access to methods and functions that will allow us to easily create a server
const express = require("express");

// Create an application using express
		// This creates an express application and stores this in a constant called app
		// In layman's terms, app is our server
const app = express();

//For our app server to run, we need a port to listen to

const port = 3000;
// Setup for allowing the server to handle data from requests
		// Allows your app to read json data
		// Methods used from express JS are middlewares
		// Middleware is software that provides common services and capabilities to applications outside of what’s offered by the operating system
		// API management is one of the common application of middlewares.
app.use(express.json())

// Allows your app to read data from forms
		// By default, information received from the url can only be received as a string or an array
		// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended: true}));




// ============ ROUTES ============
// Express has methods corresponding to each HTTP method
		// This route expects to receive a GET request at the base URI "/"
		// The full base URI for our local application for this route will be at "http://localhost:3000"
		// This route will return a simple message back to the client
app.get("/", (req, res) => {

	res.send("Hello World")
});


app.post("/hello", (req, res) => {
	// req.body contains the contents/data of the request body
	// All the properties defined in our Postman request will be accessible here as properties with the same names
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

// An array that will store user objects when the "/signup" route is accessed
		// This will serve as our mock database
let users = [];
app.post("/signup", (req, res) => {
	console.log(req.body);

	//para di tatanggap ng empty username and password
	if(req.body.username !== "" && req.body.password !== ""){
		// will store the user object sent via postman to the users array created above
		users.push(req.body);


		res.send(`User ${req.body.username} successfully registered`);
	} else {
		res.send('Please input BOTH username and password.')
	}
});

app.put("/change-password", (req, res) => {
	// Creates a variable to store the message to be sent back to the client/Postman
	let message;

	// Creates a for loop that will loop through the elements of the "users" array

	for(let i = 0; i < users.length;i++){
	
		// If the username provided in the client/Postman and the username of the current object in the loop is the same		
		if(req.body.username == users[i].username){
			//Changes the password of the user found by the loop into the password provided
			users[i].password == req.body.password;


			message = `User ${req.body.username}'s password has ben updated`;

			break;
		} else {
			message = "User does not exist.";
		}
	}
	res.send(message);
});


users.forEach((user) => {
	    // If the username provided in the client/Postman and the username of the current object in the loop is the same
	    if (req.body.username === user.username) {
	      // Changes the password of the user found by the loop into the password provided in the client/Postman
	      user.password = req.body.password;

	      // Changes the message to be sent back by the response
	      message = `User ${req.body.username}'s password has been updated.`;
	    } else {
	    	// Changes the message to be sent back by the response
	    	message = "User does not exist.";
	    }
	  });


// ====================== ACTIVITY ===================


















app.listen(port, () => console.log(`Server running ar port ${port}.`));