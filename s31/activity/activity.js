// - What directive is used by Node.js in loading the modules it needs?
// Answer: require



// - What Node.js module contains a method for server creation?
// Answer: http module



// - What is the method of the http object responsible for creating a server using Node.js?
// Answer: http.createServer()



// - Where will console.log() output its contents when run in Node.js?
// - Answer: terminal



// - What method of the response object allows us to set status codes and content types?
// - Answer: writeHead()


// - What property of the request object contains the address's endpoint?
// - Answer: url 
