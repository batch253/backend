// CRUD Operations
		/*
		    - CRUD operations are the heart of any backend application.
		    - Mastering the CRUD operations is essential for any developer.
		    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
		    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
		*/

//Insert documents (Create)
/*
Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			});

		Insert Many Documents
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}
			]);

*/
db.users.insertOne({
	"firstName" : "Janie",
	"lastName"	: "Jardeleza",
	"mobileNumber" : "+639204071836",
	"email" : "janiejardeleza",
	"company": "Zuitt"
});

db.user.insertMany([
		{
			firstName	: "Stephen",
			lastName	: "Hawking",
			age			: 76,
			contact 	:{
				phone	: "875452221",
				email	: "stephenhawking@mail.com"
			} ,
			courses: ["Python", "React", "PHP", "CSS"],
			department : "none"
		},

		{
			firstName	: "Neil",
			lastName	: "Armstrong",
			age			: 82,
			contact 	:{
				phone	: "98765432",
				email	: "neilarmstrong@mail.com"
			} ,
			courses: ["Sass", "React", "Laravel"],
			department : "none"
		}
	]);



//FINDING DOCUMENT (Read/Retrieve)
/*
	Syntax:
	 	-db.collectionName.find() - find all
	 	-db.collectionName.find({field: value});

*/

db.users.find();
db.users.find({"firstName" : "Stephen"})

db.courses.insertMany([
		{
			name: "Javascript 101"
			price: 5000
			description: "Introduction to Javascript"
			isActive: true

			
		},

		{
			name: "HTML 101"
			price: 2000
			description: "Introduction to HTML"
			isActive: true
		}
	]);


//Finding documents (Read/Retrieve)
/*
	Syntax:
        - db.collectionName.find() - find all
        - db.collectionName.find({field: value}); - all document that will match the criteriea
        - db.collectionName.findOne({field: value}) - first document that will mathc the criteria
        - db.collectionName.findOne({}) - find first documents

*/

db.users.find();
db.users.find({"firstName" : "Stephen"});
db.users.findOne({});


// Updating/Replicing/Modifying documents (update)

/*
		Syntax:
			db.collectionName.updateOne(
				{
					field: value
				},
				{
					$set: {
						fieldToBeUpdated: value
					}
				}
			);
			- update the first matching document in our collection


	Multiple/Many Documents
	db.collectionName.updateMany()
*/

db.users.insertOne({
	"firstName" : "Test",
	"lastName" : "Test",
	"mobileNumber" : "+639204071836",
	"email" : "test@mail.com",
	"company" : "none",
}):

db.users.updateOne(
    {
        "firstName": "test"
    },
    {
        $set: {
        "firstName": "Bill",
        "lastName": "Gates",
        "mobileNumber": "123456789",
        "email": "bilgates@mail.com",
        "company": "Microsoft",
        "status": "active"
    	}
    }
);

db.courses.updateOne(
    {
       
    }
);



/*
	Syntax:
		Deleting One Document:
			db.collectionName.deleteOne({"critera": "value"})

		Deleting Multiple Documents:
			db.collectionName.deleteMany({"criteria": "value"})


*/

