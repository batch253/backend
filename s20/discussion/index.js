// console.log("Hi!")

// // LOOPS
// 	//WHile Loop
// 	/*
// 			- A while loop takes in an expression/condition
// 			- Expressions are any unit of code that can be evaluated to a value
// 			- If the condition evaluates to true, the statements inside the code block will be executed
// 			- A statement is a command that the programmer gives to the computer
// 			- A loop will iterate a certain number of times until an expression/condition is met
// 			- "Iteration" is the term given to the repetition of statements

// 			Syntax:
// 				while(expression/condition) {
// 					statement;
// 				}
// 	*/

// let count = 5 ;

// //while the value of count is not equal to 0
// while (count !== 0) {
// 	console.log("While: " + count);
// 	count --
// }


// let i =15

// while (i !== 0) {
// 	console.log("Hi " + i)
// 	i --
// }

// /*
// 		- A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

// 		Syntax:
// 			do {
// 				statement
// 			} while (expression/condition)
// */


// 	let number = Number(prompt("Give me a number"));

// 	do{
// 		console.log("Do While: " + number);

// 		number += 1
// 	} while (number < 10);


//For Loop

/*
	- A for loop is more flexible than while and do-while loops. It consists of three parts:
	    1. The "initialization" value that will track the progression of the loop.
	    2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
	    3. The "finalExpression" indicates how to advance the loop.

	    Syntax:
	    for(initalization/initial value; expression/condition;iteration)
*/

for (let n = 0; n <= 20; n+=2) {

	//The current value of n is printed out
	console.log(n)
}

let myString = "Iamadeveloper";

console.log(myString.length);

// console.log(myString[0]);
// console.log(myString[1]);
// console.log(myString[2]);
// console.log(myString[3]);

for (let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}


let givenName = "rOmMeL"
let vowel = 0;

// for (let x = 0; x < givenName.length; x++) {
// 	if (
// 		givenName[x] === "a" || 
// 		givenName[x] === "e" || 
// 		givenName[x] === "i" || 
// 		givenName[x] === "o" || 
// 		givenName[x] === "u" 
// 		) {
// 	vowel++
// 		}
// }
// console.log(vowel)


// for (let i = 0; i < givenName.length; i++){
// 	if (
// 		givenName[i].toLowerCase() == "a" || 
// 		givenName[i].toLowerCase() == "e" || 
// 		givenName[i].toLowerCase() == "i" || 
// 		givenName[i].toLowerCase() == "o" || 
// 		givenName[i].toLowerCase() == "u" 
// 	) {
// 		console.log()
// 	} else {

// 		console.log(givenName[i])
// 	}
// }



for (let  count = 0; count <= 20; count++) {
	if(count % 2 === 0) {
		continue;
	}
	console.log("Continue adn Break: " + count)

	if (count > 10) {
		break;
	}
}

let name = "alejandro"

for (let i = 0; i < name.length; i++) {

	console.log(name[i]);

	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration");
		continue;
	}
	if(name[i] == "d") {
		break;
	}
}

