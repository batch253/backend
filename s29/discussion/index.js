db.inventory.insertMany([
		{
			"name": "Javascript for Beginners",
			"author": "James Doe",
			"price": 5000,
			"stocks": 50,
			"publisher": "JS Publishing House"
		},
		{
			"name": "HTML and CSS",
			"author": "John Thomas",
			"price": 2500,
			"stocks": 38,
			"publisher": "NY Publishers"
		},
		{
			"name": "Web Development Fundamentals",
			"author": "Noah Jimenez",
			"price": 3000,
			"stocks": 10,
			"publisher": "Big Idea Publishing"
		},
		{
			"name": "Java Programming",
			"author": "David Michael",
			"price": 10000,
			"stocks": 100,
			"publisher": "JS Publishing House"
		}
	]);


//COMPARISON QUERY OPERATORS
//$gt operator - matches the values that are greater than a specified value.
//$gte operator - matches the value that are greater than or equal to a specified value
/*
	Syntax :
		for $gt:
				
				db.collectionName.find(
					{
						"field" : {$gt: "value"}
					}
				);
			for $gte:
							
							db.collectionName.find(
								{
									"field" : {$gt: "value"}
								}
							);
	*/

db.inventory.find(
	{
		"stocks" : {$gte:50}
	}
);

db.inventory.find(
	{
		"stocks" : {$gt:50}
	}
)

//$gt operator - matches the values that are less than a specified value.
//$gte operator - matches the value that are less than or equal to a specified value
/*
Syntax :
	for $lt:
			
			db.collectionName.find(
				{
					"field" : {$lt: "value"}
				}
			);
		for $lte:
			
			db.collectionName.find(
				{
					"field" : {$lte: "value"}
				}
			);
*/


db.inventory.find(
	{
		"stocks" : {$lt:50}
	}
)

//$ne operator - matches the values that are not equal a specified value.
/*
Syntax :
	for $ne:
			
			db.collectionName.find(
				{
					"field" : {$lt: "value"}
				}
			);
*/

db.inventory.find(
	{
		"stocks" : {$ne:50}
	}
);


//$eq operator - matches the values that are not equal a specified value.
/*
Syntax :
	for $eq:
			
			db.collectionName.find(
				{
					"field" : {$lt: "value"}
				}
			);
*/

db.inventory.find(
	{
		"stocks" : {$eq:50}
	}
);

//$in operator - matches the values specified in an array.
/*
Syntax :
	for $eq:
			
			db.collectionName.find(
				{
					"field" : {$in: [Value1, Value2]}
				}
			);
*/

db.inventory.find(
	{
		"price" : {$in:[10000,5000]}
	}
);



//$nin operator - matches the values specified in an array.
/*
Syntax :
	for $nin:
			
			db.collectionName.find(
				{
					"field" : {$nin: [Value1, Value2]}
				}
			);
*/

db.inventory.find(
	{
		"price" : {$nin:[10000,5000]}
	}
);

db.inventory.find(
	{
		"price" : {$gt:2000} && {$lt:4000}
	}
);


//LOGICAL OPERATOR

db.inventory.find({
	$or: [{
		{"name": "HTML and CSS"},
		{"publisher": "JS Publishing House"}
		]
});

 
db.inventory.find({
	$or: [
		{"author": "James Doe"},
		{"price": 
			{$lte : 5000
		}}
		]
});


 
db.inventory.find({
	$or: [
		{"stocks" {
			$nin : [10, 50]
		}},
		{"stocks": {
			$gte : 50
		}
		}}
	]
});


db.inventory.find({
	$and: [
		{
			"stocks": {
			$ne : 50
		}},
		{"price": {
			$ne : 5000
		}}
	]
});

//FIELD PROJECTION
// INCLUSION - Matches teh document accrording to the given cruteria and returns included fields

/*
		Syntax ;
		db.collectionName.find(
				{criteria (teh key-value pair)},
				{"field":1}
		);

*/

// Inclusion - Matches the document according to the given criteria and returns included field/s.

/*
    Syntax:
        db.collectionName.find(
            { criteria (the key-value pair) },
            { "field": 1}
        );
*/

		db.inventory.find(
				{
					"publisher": "JS Publishing House", 
				},
				{
					"name":1,
					"author":1,
					"price":1
				}
		);


// Exclusion - Matches documents with given criteria and returns fields that were not excluded

		db.inventory.find(
				{
					"author": "Noah Jimenez", 
				},
				{
					"price":0,
					"stocks":0,
				}
		);

		db.inventory.find(
				{
					"price": {$lte:5000}, 
				},
				{
					"_id":0,
					"name":1,
					"author":1,
				}
		);

// $regex operator (regular expression) - selects documents where values match a specified regular expression.

/*
	Syntax :
		db.collectionName.find(
			{
				"field":$regex : {'pattern',
						$options: "optionvalue"}
			}
		);	

	*/

// Only find fields with capital M
db.inventory.find(
			{
				"author":{ 
					$regex : 'M',
				}
			});	

db.inventory.find(
			{
				"author":{ 
					$regex : 'M',
					$options: '$i'
				}
			});	


/*
	Mini Activity:
		- in the inventory collections, find a "name" of a book/s with letters "java" in it and has a "price" of greater than or equal to 5000.
			-- use $and, $regex and $gte operators

*/
db.inventory.find(
			{
				"name":{ 
					$regex : 'java',
					$options: '$i'
				}
			});	

db.inventory.find({
	$and: [
			{
				"name":{ 
				$regex : 'java'
			}
		};	
		{
			"price": {$gte : 5000}
		}
	]	
	});

//UPDATE


db.inventory.updateOne(
	{
		"price" : {
			$gte : 3000
		}
	},
	{
		$set: {
			"stocks" : 100
		}
	}
);

//DELETEONE

db.inventory.deleteOne(
	{
		"price" : {
			$gte : 3000
		}
	}
);

