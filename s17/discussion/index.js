// console.log("Hello")
//[ SECTION ] Functions
	// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function
//Function declarations
	//(function statement) defines a function with the specified parameters.

	/*
	Syntax:
		function functionName() {
			code block (statement)
		}
	*/
	// function keyword - used to defined a javascript functions
	// functionName - the function name. Functions are named to be able to use later in the code.
	// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.


	function printName() {
		console.log("My name is Rommel");
	};

	//[ SECTION ] Function Invocation
		//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
		//It is common to use the term "call a function" instead of "invoke a function".
	printName();

	//Semicolons are used to separate executable JavaScript statements.

	//Function Declarations

			//A function can be created through function declaration by using the function keyword and adding a function name.

			//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).
			
			//declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.
			//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.


function declaredFunction() {
	console.log("Hello from declaredFunction()!");
};

declaredFunction()

	//Function Expression
		//A function can also be stored in a variable. This is called a function expression.

		//A function expression is an anonymous function assigned to the variableFunction

		//Anonymous function - a function without a name.

// variableFunction(); 

let variableFunction = function(){
	console.log("Hello again!")
}

variableFunction();

let funcExpression = function funcName() {
	console.log("Hello from the other side");
}

funcExpression()

// funcName()
// funcName(); error: funcName is not defined (variable of a function must be called/invoked)

declaredFunction = function() {
	console.log("Updated declaredFunction()");
};

declaredFunction(); // declared functions can be reassigned

funcExpression = function() {
	console.log("Updated funcExpression")
};

funcExpression(); // either function declaration/expression can be reassigned

// cons constantFunc = function(){
// 	console.log("Initialized with const.");
// };

// constantFunc();

//[SECTION]  Function Scoping

/*	
	Scope is the accessibility (visibility) of variables.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
			JavaScript has function scope: Each function creates a new scope.
			Variables defined inside a function are not accessible (visible) from outside the function.
			Variables declared with var, let and const are quite similar when declared inside a function
*/

{
	let localVar = "Kudo Shinichi";
};
let globalVar = "Ms. Universe";

console.log(globalVar);
// console.log(localVar); should be inside scope to run

function showNames() {

	var functionVar = "Blossom";
	let functionLet = "Bubbles";
	const functionConst = "Buttercup";

	console.log(functionVar);
	console.log(functionLet);
	console.log(functionConst);
};

showNames();
/*
 Error: variableName not defined becase the variable are inside the fucntion scope
 */

	// console.log(functionVar);
	// console.log(functionLet);
	// console.log(functionConst);


// Nested Functions
// Nested Functions are functions that are inside a function. It will have acces to the variable name as they are within the same scope/code block.

/*function myNewFunc() {
	let name1 = "Jane";

	function nestedFunc(){
		let nestedName = "John";

		console.log(name1);
	}
	// console.log(nestedName); //   console.log(nestedName); Error: not defined (global variables are only accessible inside the child. local variables cannot be accessed outside the scope)
	nestedFunc()
};

myNewFunc();

// nestedFunc() : Error: not defined (global variables are only accessible inside the child. local variables cannot be accessed outside the scope)

console.log(name1);*/

//[ SECTION ] Using alert()

	//alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

	// alert("Hello Batch 253!");
	//This will run immediately when the page loads.
	//alert() can be ised to show a message to the user form a later function
	function showSampleAlert() {
		alert("Hello, student!");
	};


	showSampleAlert();

	console.log("Will only display when alert dismmissed.");

	//Notes on the use of alert():
	//Show only an alert() for short dialogs/messages to the user. 
	//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.
	//prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

// let  samplePrompt = prompt("Enter your name:");

// console.log("Hello, " + samplePrompt);
// /prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().
/*
	Syntax:
		prompt("<dialogInString>");
*/




// function printWelcome() {
// 	let promptFirstName = prompt("Enter your first name:");
// 	let promptLastName = prompt("Enter your Last name");
// 	console.log("Hello " + promptFirstName + promptLastName)
// };

// printWelcome();



//[SECTION] Function Naming Conventions
// Function names should be definitive of the tasl it will perform. It usually contains a verb.

function getFaveAnime() {
	let animes = ["Magi: The Labyrinth of Magic","Hunter x Hunter", "Your Name" ];	
		console.log(animes);
}

getFaveAnime();

// Avoid generic names to avoid confusion within your code.

function get() {
	let pet = "cat"
	console.log(pet);
};

get();


//Avoid pointless and inappropriate function names.
//Name your functions in small caps. Follow camelCase when naming varibales and functions


function a(){
	console.log("A");
};

a();