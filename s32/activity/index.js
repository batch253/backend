let http = require("http");

let port = 4000;

http.createServer(function(request, response) {

	// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
	// The method "GET" means that we will be retrieving or reading information
	if(request.url == "/" && request.method == "GET"){
		// Requests the "/items" path and "GETS" information 
		response.writeHead(200, { "Content-Type": "text/plain" });
		// Ends the response process
		response.end("Welcome to the Booking System");

		// The method "POST" means that we will be adding or creating information
		// In this example, we will just be sending a text response for now
	
	} else if (request.url == "/profile" && request.method == "GET"){
		// Requests the "/items" path and "SENDS" information
		response.writeHead(200, { "Content-Type": 'text/plain' });
		response.end("Welcome to your profile!");
	
	}  else if (request.url == "/courses" && request.method == "GET"){
		// Requests the "/items" path and "SENDS" information
		response.writeHead(200, { "Content-Type": 'text/plain' });
		response.end("Here’s our courses available");
	
	}  else if (request.url == "/addCourse" && request.method == "POST"){
		// Requests the "/items" path and "SENDS" information
		response.writeHead(200, { "Content-Type": 'text/plain' });
		response.end("Add a course to our resources");
	
	}  else if (request.url == "/updateCourse" && request.method == "PUT"){
		// Requests the "/items" path and "SENDS" information
		response.writeHead(200, { "Content-Type": 'text/plain' });
		response.end("Update a course to our resources");
	}  else if (request.url == "/archiveCourse" && request.method == "DELETE"){
		// Requests the "/items" path and "SENDS" information
		response.writeHead(200, { "Content-Type": 'text/plain' });
		response.end("Archive courses to our resources");
	}

}).listen(port);

console.log("Running at localhost:4000");
