const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");
const auth = require("../auth");

router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}

});

router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.getAllProduct().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});

router.get("/allactive", auth.verify, (req, res) => {
		productController.getAllActive().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

router.get("/:productId", (req, res) => {
		productController.getSpecificProduct(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});


router.put("/:productId/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});

router.put("/:productId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});
router.put("/:productId/activate", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});





module.exports = router;