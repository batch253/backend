const express = require("express");
const router = express.Router();

const orderController = require("../controllers/orderController");
const auth = require("../auth");

router.post("/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	let data = {
		userID: req.body.userId,
		isAdmin: userData.isAdmin,
		productId: req.body.productId,
		quantity: req.body.quantity,
		totalAmount:req.body.totalAmount

	}

	if(!userData.isAdmin){
		orderController.createOrder(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}

});

module.exports = router;
