const Product = require("../models/Product");

module.exports.createProduct = (reqBody) => {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});

		return newProduct.save().then(product => true)
			.catch(err => false);
};

module.exports.getAllProduct = () => {
	return Product.find({}).then(result => result)
		.catch(err => err);
};

module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => result)
		.catch(err => err);
};


module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => result)
		.catch(err => err);
};

module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct= {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
		.then(product => true).catch(err => err);

};

module.exports.archiveProduct = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive	
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(product => true).catch(err => err);
};

module.exports.activateProduct = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive	
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(product => true).catch(err => err);
};
