console.log("Halu");

//Array
//Array in programming is simply a list of data

let studentNumberA = "2023-1921"
let studentNumberB = "2023-1922"
let studentNumberC = "2023-1923"
let studentNumberD = "2023-1924"
let studentNumbers = ["2023-1921","2023-1922", "2023-1923","2023-1924"]


/*
	- Arrays are used to store multiple related values in a single variable
	- They are declared using square brackets ([]) also known as "Array Literals"
	- Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
	- Arrays also provide access to a number of functions/methods that help in achieving this
	- A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
	- Majority of methods are used to manipulate information stored within the same object
	- Arrays are also objects which is another data type
	- The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"


		Syntax:
		let/const arrayName = [elementA, elementB, elementC, ...];
*/

//Common examples of arrays
let grades = [98.5, 94.3, 89.2 ,90.1]; //number array
let computerBrands = ["Acer", "Asus", "Lenovo", "MSI"]; //string array
//not recommended use of array
let mixedArray = [12, "Asus", null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArray);

let myTasks = [
	'drink html', 
	'eat javascript', 
	'inhale css', 
	'bake sass'
];

let city1 ="Tokyo", city2="Manila", city3 = "Berlin";

//Creating an array woth values from variables
let cities =[city1, city2, city3]

console.log(myTasks)
console.log(cities)

//[SECTION] Length property
//THe .length property allows us to get and set the total number of items/elements in an array

console.log(myTasks.length)
console.log(cities.length)

// Length property in string allows to get the total number of characters including the white spaces

let fullName = "Sana Minatozaki"; 
console.log(fullName.length);

let blankArray = []; 
console.log(blankArray.length); 
console.log(blankArray);

//deletiing last item of an array
myTasks.length = myTasks.length-1; 
console.log(myTasks.length); 
console.log(myTasks);

//using decrementation
cities.length--; 
console.log(cities);

//We cannot delete using .length property in strings
fullName.length = fullName.length-1;
console.log(fullName.length)

//deletes an element inside an array
fullName.length--;
console.log(cities);


//lengthen array
let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles.length);
theBeatles.length++;
console.log(theBeatles);
console.log(theBeatles.length);
console.log(theBeatles.length-1);

//Assigning a value to a specific array index
//arrayName[index] = "new value";
theBeatles[4] = "Sir Jimbo";
console.log(theBeatles);


console.log(theBeatles[2]);
console.log(myTasks[0]);

//Not recommended [practice]
// console.log(grades[20]);
// grades[20] = 90.5;
// console.log(grades[20]);
// console.log(grades);

let kaponanNiEugene = ["Eugene", "Vincent", "Alfred", "Dennis"];
console.log(kaponanNiEugene[3]);
console.log(kaponanNiEugene[1]);

let member = kaponanNiEugene[2];
console.log(member);

console.log("Array before reassignment:"); 
console.log(kaponanNiEugene); 
kaponanNiEugene [2]= "Jeremiah"; 
console.log("Array after reassignment: "); 
console.log(kaponanNiEugene);

let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegend.length-1; 
console.log(bullsLegend [lastElementIndex]);

// Adding items into the array 
let newArr = []; 
console.log(newArr[0]); 
console.log(newArr);

console.log(newArr);
console.log(newArr);
newArr[1] = "Mina";

// This is to add an element at the end of the array console.log(newArr.length);

console.log(newArr);
newArr [newArr.length] = "Momo"; 
console.log(newArr.length); 
newArr[newArr.length] = "Sana"; 
console.log(newArr);
newArr[0]= "Tzuyu";


heroArr = []

function getSuperhero(x) {
	heroArr[heroArr.length] = x
	console.log(heroArr)
}


//Loops over an Array

for (let index = 0; index < newArr.length; index++) {
	console.log(newArr[index])
};

let numArr = [5, 12, 30 , 46 , 50 , 98];
for(let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0) {
		console.log(numArr[index] + "is divisible by 5.")
	}else {

	}
}

