const express = require("express");

const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin123@batch253-esteves.s0qft0l.mongodb.net/s35?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log(`We are now connected to the cloud database: MongoDB Atlas!`));

const user = new mongoose.Schema({

	username: String,
	password:{
		type: String,
	}
});

const User = mongoose.model("User", user);
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== "" && req.body.password !== ""){
		user.push(req.body);
		res.send(`New user registered`);
	} else {
		res.send('Please input BOTH username and password.')
	}
});



app.listen(port, () => console.log(`Server running at localhost: ${port}...`))